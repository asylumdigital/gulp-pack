// gulpfile.js

const gulp = require('gulp');
const sass = require('gulp-sass');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const webpackConfig = require('./webpack.config.js');
const nodeSassGlobbing = require('node-sass-globbing');
const modernizr = require('gulp-modernizr-build')
const uglify = require('gulp-uglify');
const autoprefixer = require('gulp-autoprefixer');
const livereload = require('gulp-livereload');

gulp.task('sass', () => {
  return gulp.src('./src/sass/**/*.scss')
    .pipe(sass({
      sourceComments: true,
      includePaths: [
        'node_modules/'
      ],
      importer: nodeSassGlobbing
    }).on('error', sass.logError))
    .pipe(gulp.dest('./public/assets/css'))
    .pipe(livereload());

});

gulp.task('autoprefix', () => {
    gulp.src('assets/css/main.css')
        .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./public/assets/css'));
});

gulp.task('js', () => {
  gulp.src('./src/js/main.js')
    .pipe(webpackStream(webpackConfig), webpack)
    .pipe(gulp.dest('./public/assets/js'))
    .pipe(livereload());
});

gulp.task('files', () => {
  gulp.src('public/**/*.html')
    .pipe(livereload());
});

gulp.task('modernizr', () => {
  gulp.src([
      'assets/**/*.js',
      'assets/**/*.css',
      '!./**/modernizr*.js']) // Don't forget to exclude any other Modernizr files you may have in your sources.
      .pipe( modernizr('modernizr.js') )
      .pipe( uglify() ) // this plugin won't do it for  you.
      .pipe(gulp.dest('./public/assets/js'));
});

gulp.task('watch', () => {
  livereload.listen({
    port: 35729
  });
  gulp.watch(['src/sass/**/*.scss'], ['sass']);
  gulp.watch(['public/assets/css/**/*.css'], ['autoprefix']);
  gulp.watch(['src/js/**/*.js'], ['js']);
  gulp.watch(['public/**/*.html'], []);
});

gulp.task('default', [ 'sass', 'js', 'watch' ]);