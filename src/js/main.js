import testing from './modules/test';
import Mark from 'markup-js';

class AutoTrader {
  constructor(){
    this.init();

  }

  init(){

    let data = {
      Years: [
        2017,
        2018
      ],
      Results: [
        {
          Name: "Name 1",
          Image: 'http://placehold.it/250x250'
        },
        {
          Name: "Name 2",
          Image: 'http://placehold.it/350x350'
        }
      ]
    }


    document.querySelector('.destination').innerHTML = this.template('panel', data);

  }

  template(tmpl, data){
    console.log('IN');
    let template = document.querySelector(`[data-template="${tmpl}"]`);

    // console.log(template.textContent);

    if(template){
      return Mark.up(template.textContent, data);
    }

    return false;
  }
}

document.addEventListener('DOMContentLoaded', () => {
  new AutoTrader;
});
